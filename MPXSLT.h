//
//  MPXSLT.h
//  Manuscripts
//
//  Created by Markus on 1/25/13.
//  Copyright (c) 2015 Manuscripts.app Limited. All rights reserved.
//


#import <Foundation/Foundation.h>


extern NSString *_Nonnull const MPXSLTErrorDomain;


typedef NS_ENUM(NSUInteger, MPXSLTErrorCode) {
    MPXSLTErrorCodeInputXMLDataParseError = 1000,
    MPXSLTErrorCodeStylesheetParseError = 1001,
    MPXSLTErrorCodeApplyStylesheetError = 1002,
    MPXSLTErrorCodeTimeoutError = 1003,
    MPXSLTErrorCodeInvalidatedConnection = 1004
};


@interface MPXSLT : NSObject

/**
 * Should be called once in app startup to ensure the first action that needs the XSL transform XPC service doesn't take multiple seconds to complete.
 */
+ (void)prepareXSLTransformServiceIfNeeded;

+ (nullable NSString *)XMLStringFromXMLString:(nonnull NSString *)inputXMLString
                  byApplyingXSLTransformNamed:(nonnull NSString *)transformName
                                   parameters:(nullable NSDictionary *)parameters
                                        error:(NSError *_Nullable *_Nullable)error;

+ (nullable NSString *)XMLStringWithContentsOfURL:(nonnull NSURL *)url
                      byApplyingXSLTransformNamed:(nonnull NSString *)transformName
                                       parameters:(nullable NSDictionary *)parameters
                                            error:(NSError *_Nullable *_Nullable)error;

- (nonnull instancetype)init NS_UNAVAILABLE;

@end
