//
//  MPXSLTransformService.h
//  MPXSLTransformService
//
//  Created by Markus Piipari on 05/08/15.
//  Copyright (c) 2015 Manuscripts.app Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MPXSLTransformServiceProtocol.h"


@interface MPXSLTransformService : NSObject <MPXSLTransformServiceProtocol>
@end
