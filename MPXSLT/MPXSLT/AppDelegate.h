//
//  AppDelegate.h
//  MPXSLT
//
//  Created by Matias Piipari on 25/05/2017.
//  Copyright © 2017 Manuscripts.app Limited. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

