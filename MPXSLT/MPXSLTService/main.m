//
//  main.m
//  MPXSLTransformService
//
//  Created by Markus Piipari on 05/08/15.
//  Copyright (c) 2015 Manuscripts.app Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MPXSLTransformService.h"

@interface ServiceDelegate : NSObject <NSXPCListenerDelegate>
@end

@implementation ServiceDelegate

- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection
{
    newConnection.exportedInterface = [NSXPCInterface interfaceWithProtocol:@protocol(MPXSLTransformServiceProtocol)];
    MPXSLTransformService *exportedObject = [MPXSLTransformService new];
    newConnection.exportedObject = exportedObject;
    
    [newConnection resume];
    return YES;
}

@end

int main(int argc, const char *argv[])
{
    ServiceDelegate *delegate = [ServiceDelegate new];
    NSXPCListener *listener = [NSXPCListener serviceListener];
    listener.delegate = delegate;
    
    // Resuming the serviceListener starts this service. This method does not return.
    [listener resume];
    return 0;
}
