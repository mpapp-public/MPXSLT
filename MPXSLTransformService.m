//
//  MPXSLTransformService.m
//  MPXSLTransformService
//
//  Created by Markus Piipari on 05/08/15.
//  Copyright (c) 2015 Manuscripts.app Limited. All rights reserved.
//


#import "MPXSLTransformService.h"
#import "MPXSLT.h"

@implementation MPXSLTransformService

- (void)XMLStringFromXMLString:(NSString *)inputXMLString
   byApplyingXSLTransformNamed:(NSString *)transformName
                    parameters:(NSDictionary *)parameters
             completionHandler:(MPXSLTransformCompletionHandler)completionHandler
{
    NSParameterAssert(completionHandler != nil);
    
    NSError *error = nil;
    NSString *result = [MPXSLT XMLStringFromXMLString:inputXMLString byApplyingXSLTransformNamed:transformName parameters:parameters error:&error];
    
    if (completionHandler) {
        completionHandler(result, error);
    }
}

@end
