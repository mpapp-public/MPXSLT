//
//  MPXSLT.m
//  Manuscripts
//
//  Created by Markus on 1/25/13.
//  Copyright (c) 2015 Manuscripts.app Limited. All rights reserved.
//

#import "MPXSLT.h"

#import "MPXSLTransformServiceProtocol.h"

#import <libxml/xmlmemory.h>
#import <libxml/debugXML.h>
#import <libxml/HTMLtree.h>
#import <libxml/xmlIO.h>
#import <libxml/xinclude.h>
#import <libxml/catalog.h>
#import <libxslt/xslt.h>
#import <libxslt/documents.h>
#import <libxslt/xsltInternals.h>
#import <libxml/parserInternals.h>
#import <libxslt/xsltutils.h>
#import <libxslt/transform.h>
#import <libexslt/exslt.h>

#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonCryptor.h>

@interface NSBundle (MPXSLT)

+ (BOOL)mpxslt_isXPCService;

@end

@implementation NSBundle (MPXSLT)

+ (BOOL)mpxslt_isXPCService
{
    BOOL b = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundlePackageType"] isEqualToString:@"XPC!"];
    return b;
}

@end

@interface NSFileManager (MPXSLT)
- (NSArray *)mpxslt_recursivePathsForResourcesOfType:(NSString *)type inDirectory:(NSString *)directoryPath;
@end

@implementation NSFileManager (MPXSLT)

// taken from http://stackoverflow.com/questions/5836587/how-do-i-get-all-resource-paths-in-my-bundle-recursively-in-ios
- (NSArray *)mpxslt_recursivePathsForResourcesOfType:(NSString *)type inDirectory:(NSString *)directoryPath {
    
    NSMutableArray *filePaths = [NSMutableArray new];
    
    // Enumerators are recursive
    NSDirectoryEnumerator *enumerator = [self enumeratorAtPath:directoryPath];
    
    NSString *filePath;
    
    while ((filePath = [enumerator nextObject]) != nil) {
        
        // If we have the right type of file, add it to the list
        // Make sure to prepend the directory path
        if ([[filePath pathExtension] isEqualToString:type] || !type) {
            [filePaths addObject:[directoryPath stringByAppendingPathComponent:filePath]];
        }
    }
    
    return filePaths.copy;
}

@end

@interface NSData (Encryption)
- (NSData *)mpxslt_AES256DecryptWithKey:(NSString*)key;
@end

@implementation NSData (Encryption)

- (NSData *)mpxslt_AES256DecryptWithKey:(NSString*)key {
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySizeAES256 + 1]; // room for terminator (unused)
    bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [self length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize           = dataLength + kCCBlockSizeAES128;
    void* buffer                = malloc(bufferSize);
    
    size_t numBytesDecrypted    = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeAES256,
                                          NULL /* initialization vector (optional) */,
                                          [self bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesDecrypted);
    
    if (cryptStatus == kCCSuccess)
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    }
    
    free(buffer); //free the buffer;
    return nil;
}


@end


NSString *const MPXSLTErrorDomain = @"MPXSLTErrorDomain";

static void MPXSLTReportError(MPXSLTErrorCode code, NSString *description, NSError *__autoreleasing *error)
{
    if (error != NULL) {
        NSMutableString *reason = [NSMutableString string];
        xmlErrorPtr XMLError = xmlGetLastError();

        if (XMLError != NULL)
        {
            if (XMLError->message != NULL) [reason appendString:@(XMLError->message)];
            
            if (XMLError->str1 != NULL)
            {
                if (reason.length > 0) [reason appendString:@"\n"];
                [reason appendString:@(XMLError->str1)];
            }
            
            if (XMLError->str2 != NULL)
            {
                if (reason.length > 0) [reason appendString:@"\n"];
                [reason appendString:@(XMLError->str2)];
            }
            
            if (XMLError->str3 != NULL)
            {
                if (reason.length > 0) [reason appendString:@"\n"];
                [reason appendString:@(XMLError->str3)];
            }
            
            if (XMLError->line > 0) {
                if (reason.length > 0) [reason appendString:@"\n"];
                [reason appendFormat:@"Line %i", XMLError->line];
            }
                        
            if (XMLError->int2 > 0) {
                if (reason.length > 0) [reason appendString:@"\n"];
                [reason appendFormat:@"Column %i", XMLError->int2];
            }
        }
        
        *error = [NSError errorWithDomain:MPXSLTErrorDomain
                                     code:MPXSLTErrorCodeStylesheetParseError
                                 userInfo:@{NSLocalizedDescriptionKey: description, NSLocalizedFailureReasonErrorKey: reason}];
    }
}

/** Return a C string that the caller is responsible for freeing by calling good old `free()` when no longer needed. */
static char *MPCopyCStringFromStringUsingUTF8Encoding(NSString *s)
{
    size_t size = [s lengthOfBytesUsingEncoding:NSUTF8StringEncoding] + 1;
    char *CString = malloc(size);
    if (![s getCString:CString maxLength:size encoding:NSUTF8StringEncoding])
    {
        free(CString);
        return NULL;
    }
    return CString;
}

@implementation MPXSLT

+ (BOOL)shouldUseXSLTransformService
{
    static BOOL shouldCheckMainBundleKey = YES;
    static BOOL useXPC = NO;
    if (shouldCheckMainBundleKey)
    {
        useXPC = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"MPUseXSLTransformService"] boolValue];
        shouldCheckMainBundleKey = NO;
    }
    return useXPC;
}

+ (NSDictionary *)XSLTURLDirectory {
    static NSDictionary *dict = nil;
    if (!dict) {
        NSArray *paths = [NSFileManager.defaultManager mpxslt_recursivePathsForResourcesOfType:@"xsl"
                                                                            inDirectory:[NSBundle bundleForClass:self.class].resourcePath];
        NSArray *morePaths = [NSFileManager.defaultManager mpxslt_recursivePathsForResourcesOfType:@"xslt"
                                                                                inDirectory:[NSBundle bundleForClass:self.class].resourcePath];
        
        
        paths = [paths arrayByAddingObjectsFromArray:morePaths];
        
        NSMutableDictionary *d = [NSMutableDictionary new];
        for (NSString *path in paths) {
            NSString *lastPathComponent = path.lastPathComponent;
            
            NSURL *fileURL = [NSURL fileURLWithPath:path];
            
#ifdef DEBUG
            NSAssert(!d[lastPathComponent] || [d[lastPathComponent] isEqual:fileURL],
                     @"XSL filename '%@' is not unique (pre-existing: %@)", path, [d[lastPathComponent] path]);
#endif
            
            if (!d[lastPathComponent]) {
                d[lastPathComponent] = fileURL;
            }
        }
        dict = d.copy;
    }
    
    return dict;
}

// TODO: drop the dependence on XSL templates being unique by their last path component.
//       (the information about the relative path from which the template is being loaded shuold be accessible somehow via the dict pr).

static xmlDocPtr encryptedXSLTDocLoader(const xmlChar *URI,
                                 xmlDictPtr dict,
                                 int options,
                                 void *ctxt,
                                 xsltLoadType type)
{
    NSURL *URL = [NSURL fileURLWithPath:[NSString stringWithUTF8String:(char *)URI]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:URL.path]) {
        NSURL *foundURL = [MPXSLT XSLTURLDirectory][URL.lastPathComponent];
        NSCAssert(foundURL, @"Failed to find XSL with URI '%@'", URL);
        URL = foundURL;
    }
    
    NSData *data = [NSData dataWithContentsOfURL:URL];
    BOOL isEncrypted = [[NSBundle.mainBundle objectForInfoDictionaryKey:@"MPResourcesEncrypted"] boolValue];
    if (isEncrypted) {
        data = [data mpxslt_AES256DecryptWithKey:@"<your encryption key>"];
    }
    
    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    char *CString = MPCopyCStringFromStringUsingUTF8Encoding(dataString);
    if (!CString) {
        return NULL;
    }
    xmlDocPtr doc = xmlParseDoc((const xmlChar *)CString);
    
    free(CString);
    return(doc);
}

+ (void)initialize {
    if (self == [MPXSLT class]) {
        xsltSetLoaderFunc(*encryptedXSLTDocLoader);
    }
}

+ (dispatch_queue_t)xslTransformQueue
{
    static dispatch_queue_t q = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        q = dispatch_queue_create([NSStringFromClass(self) UTF8String], DISPATCH_QUEUE_SERIAL);
    });
    
    return q;
}

static NSXPCConnection *__connection = nil;

+ (void)prepareXSLTransformServiceIfNeeded {
    if (![self shouldUseXSLTransformService]) { return; }
    if (__connection) { return; }
    
    NSDate *started = [NSDate new];
    NSLog(@"Initializing a new connection to MPXSLTransformService");
    
    __connection = [[NSXPCConnection alloc] initWithServiceName:@"com.manuscripts.MPXSLTransformService"];
    __connection.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(MPXSLTransformServiceProtocol)];
    
    __connection.interruptionHandler = ^{
        NSLog(@"Connection to MPXSLTransformService was interrupted");
        // We intentionally do not invalidate the connection, or drop reference to it; this is how I interpret Apple's documentation at:
        // https://developer.apple.com/library/mac/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/CreatingXPCServices.html#//apple_ref/doc/uid/10000172i-SW6-SW24
    };
    
    __connection.invalidationHandler = ^{
        NSLog(@"Connection to MPXSLTransformService was invalidated");
        __connection = nil; // The next transform attempt will create a new connection
    };
    
    [__connection resume];
    
    NSLog(@"Done with setting up new MPXSLTransformService connection (in %.2f seconds)", ([[NSDate new] timeIntervalSinceReferenceDate] - [started timeIntervalSinceReferenceDate]));
}

+ (id)XSLTransformServiceRemoteObjectWithErrorHandler:(void (^)(NSError *error))errorHandler
{
    [self prepareXSLTransformServiceIfNeeded];
    
    id remoteObject = [__connection remoteObjectProxyWithErrorHandler:errorHandler];
    return remoteObject;
}

/**
 Transforms an XML document by feeding it through an XSLT style sheet.
 
 These helped quite a bit when constructing this:
 
 http://stackoverflow.com/questions/462440/version-of-xslt-in-iphone
 https://github.com/oriontransfer/SWXMLMapping/blob/master/SWXSLTransform.m
 
 */
+ (NSString *)XMLStringFromXMLString:(NSString *)inputXMLString
         byApplyingXSLTransformNamed:(NSString *)transformName
                          parameters:(NSDictionary *)parameters
                               error:(NSError *__autoreleasing *)error
{
    if ([self shouldUseXSLTransformService])
    {
        NSTimeInterval start = [[NSDate date] timeIntervalSinceReferenceDate];
        NSLog(@"XSL transform %@ start", transformName);
        
        __block NSString *result = nil;
        __block BOOL done = NO;
        __block NSError *remoteError = nil;
        
        id remoteObject = [self XSLTransformServiceRemoteObjectWithErrorHandler:^(NSError *error) {
            remoteError = error;
        }];
        
        NSLog(@"XSL transform %@ remote connection set up, took %@ seconds", transformName, @([[NSDate date] timeIntervalSinceReferenceDate] - start));
        
        [remoteObject XMLStringFromXMLString:inputXMLString
                 byApplyingXSLTransformNamed:transformName
                                  parameters:parameters
                           completionHandler:^(NSString *output, NSError *error) {
             if (output) {
                 result = output;
             } else {
                 remoteError = error;
             }
             done = YES;
         }];
        
        NSDate *startDate = [NSDate date];
        
        while (!done)
        {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
            
            if (!done && ([startDate timeIntervalSinceNow] < -10.0))
            {
                remoteError = [NSError errorWithDomain:MPXSLTErrorDomain
                                                  code:MPXSLTErrorCodeTimeoutError
                                              userInfo:@{NSLocalizedDescriptionKey:@"Content conversion failed",
                                                         NSLocalizedRecoverySuggestionErrorKey:[NSString stringWithFormat:@"XSL transform named '%@' timed out.\n\nPlease contact support@manuscriptsapp.com for assistance, and consider attaching the document which leads to this error if applicable.", transformName]}];
                break;
            }
        }
        
        if (!result && error && remoteError) {
            *error = remoteError;
        }
        
        NSLog(@"XSL transform %@ end, took %@ seconds", transformName, @([[NSDate date] timeIntervalSinceReferenceDate] - start));
        
        return result;
    }
    else
    {
        // Requests by an XPC service are handled by a serial queue (that however is not the main thread/queue)
        if ([NSBundle mpxslt_isXPCService]) {
            NSString *XMLString = [self _XMLStringFromXMLString:inputXMLString byApplyingXSLTransformNamed:transformName parameters:parameters error:error];
            return XMLString;
        }
        
        // If we are running in the main app, must dispatch XSL execution to a serial queue
        else {
            __block NSString *xmlStr = nil;
            dispatch_sync([[self class] xslTransformQueue], ^{
                xmlStr = [self _XMLStringFromXMLString:inputXMLString byApplyingXSLTransformNamed:transformName parameters:parameters error:error];
            });
            return xmlStr;
        }
    }
}

+ (NSString *)XMLStringWithContentsOfURL:(NSURL *)url
             byApplyingXSLTransformNamed:(NSString *)transformName
                              parameters:(NSDictionary *)parameters
                                   error:(NSError *__autoreleasing *)error {
    NSString *str = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:error];
    return [self XMLStringFromXMLString:str byApplyingXSLTransformNamed:transformName parameters:parameters error:error];
}


+ (NSString *)_XMLStringFromXMLString:(NSString *)inputXMLString
          byApplyingXSLTransformNamed:(NSString *)transformName
                           parameters:(NSDictionary *)parameters
                                error:(NSError *__autoreleasing *)error
{
    static int transformCount = 0;
    transformCount++;
    
    //MPLogDebug(@"Start XSL transform round %i: %@", transformCount, transformName);
    //MPLogDebug(@"XSL transform input string:\n%@", inputXMLString);
    
    xmlThrDefLoadExtDtdDefaultValue(false);
    xmlSubstituteEntitiesDefault(true);
    xmlSetExternalEntityLoader(xmlNoNetExternalEntityLoader);
    
    xsltInit();
    
    BOOL enableXSLLogging = [[NSUserDefaults standardUserDefaults] boolForKey:@"MPXSLTLoggingEnabled"];
    xsltDebugSetDefaultTrace(enableXSLLogging ? XSLT_TRACE_ALL : XSLT_TRACE_NONE);
    
    // Load style sheet
    exsltRegisterAll();
    
    NSString *path = nil;

    NSArray *bundles = @[];
    NSBundle *appBundle = [NSBundle mainBundle];
    NSParameterAssert(appBundle != nil);
    
    if (appBundle) {
        bundles = [bundles arrayByAddingObject:appBundle];
    }
    
    NSBundle *foundationBundle = [NSBundle bundleForClass:self.class];
    NSParameterAssert(foundationBundle != nil);
    if (foundationBundle) {
        bundles = [bundles arrayByAddingObject:foundationBundle];
    }
    
    // the bundles added above may be the same object, hence using NSOrderedSet (check once, but check in order).
    for (NSBundle *bundle in [NSOrderedSet orderedSetWithArray:bundles])
    {
        path = [bundle URLForResource:transformName withExtension:@"xsl"].path;

        if (!path && ![transformName isEqualToString:transformName.lastPathComponent]) {
            path = [bundle URLForResource:transformName.lastPathComponent withExtension:@"xsl"].path;
        }
        
        if (path) {
            break;
        }
    }
    
    NSAssert(path, @"Tranform %@ not found", transformName); // Make sure stylesheet is copied into the PressRoom bundle resources by the appropriate Copy Files phase in the PressRoom target
    char *pathCString = MPCopyCStringFromStringUsingUTF8Encoding(path);
    xsltStylesheetPtr stylesheet = xsltParseStylesheetFile((const xmlChar *)pathCString);
    free(pathCString);
    pathCString = NULL;

    if (stylesheet == NULL) {
        MPXSLTReportError(MPXSLTErrorCodeStylesheetParseError,
                          [NSString stringWithFormat:@"Failed to parse XSL stylesheet at %@", path], error);
        return nil;
    }
    
    // Parse input XML document
    xmlSubstituteEntitiesDefault(false);
    char *inputCString = MPCopyCStringFromStringUsingUTF8Encoding(inputXMLString);
    if (!inputCString) {
        return nil;
    }
    
    xmlDocPtr inputDocument = xmlParseDoc((const xmlChar *)inputCString);
    if (inputDocument == NULL)
    {
        free(inputCString);
        MPXSLTReportError(MPXSLTErrorCodeInputXMLDataParseError, @"Failed to parse input XML data", error);
        return nil;
    }
    else { // the input is not needed as a C string after it was parsed into an XML document above.
        free(inputCString);
        inputCString = NULL;
    }
    
    // Set up parameters
    const char **parameterCArray = NULL;
    
    if (parameters.count > 0)
    {
        parameterCArray = calloc((parameters.count * 2) + 1, sizeof(char *));
        NSUInteger i = 0;
        
        for (NSString *key in parameters)
        {
            id value = parameters[key];
            NSString *stringValue = nil;
            
            if ([value isKindOfClass:NSString.class]) {
                stringValue = value;
            }
            else if ([value isKindOfClass:NSURL.class]) {
                stringValue = [value absoluteString];
            }
            else if ([value respondsToSelector:@selector(stringValue)]) {
                stringValue = [value stringValue];
            }
            else if ([value respondsToSelector:@selector(string)]) {
                stringValue = [value string];
            }
            else {
                stringValue = [value description];
            }
            
            //if (![stringValue hasPrefix:@"<"])
            //{
                stringValue = [NSString stringWithFormat:@"'%@'", stringValue];
            //}
            
            parameterCArray[i] = MPCopyCStringFromStringUsingUTF8Encoding(key);
            parameterCArray[i + 1] = MPCopyCStringFromStringUsingUTF8Encoding(stringValue);
            i += 2;
        }
        
        parameterCArray[i] = NULL;
    }
    
    // Apply stylesheet
    xsltTransformContextPtr transformContext = xsltNewTransformContext(stylesheet, inputDocument);
    xmlDocPtr outputDocument = xsltApplyStylesheetUser(stylesheet, inputDocument, parameters.count ? parameterCArray : NULL, NULL, stderr, transformContext);

    if (parameterCArray != NULL)
    {
        int j = 0;

        while (true)
        {
            char *s = (char *)parameterCArray[j];
            if (s) {
                free(s);
            } else {
                break;
            }
            j++;
        }
        
        free(parameterCArray);
        parameterCArray = NULL;
    }
    
    if ((transformContext->state == XSLT_STATE_ERROR) || (transformContext->state == XSLT_STATE_STOPPED))
    {
        NSString *message = [NSString stringWithFormat:@"Failed to apply XSL transform %@ to input XML data", transformName];
        
        if (outputDocument)
        {
            xmlChar *resultBuffer = NULL;
            int length = 0;
            xsltSaveResultToString(&resultBuffer, &length, outputDocument, stylesheet);
            //NSLog(@"XML output so far:\n%s", resultBuffer);
        }
        
        MPXSLTReportError(MPXSLTErrorCodeApplyStylesheetError, message, error);
        return nil;
    }
    
    xmlChar *xmlResultBuffer = NULL;
    int length = 0;
    xsltSaveResultToString(&xmlResultBuffer, &length, outputDocument, stylesheet);
    
    NSString *outputString = [[NSString alloc] initWithBytes:xmlResultBuffer length:length encoding:NSUTF8StringEncoding];
    
    // Clean up and go home
    xsltFreeTransformContext(transformContext), transformContext = NULL;
    xsltFreeStylesheet(stylesheet), stylesheet = NULL;
    xmlFreeDoc(outputDocument), outputDocument = NULL;
    xmlFreeDoc(inputDocument), inputDocument = NULL;
    free(xmlResultBuffer), xmlResultBuffer = NULL;

    xsltCleanupGlobals();
    xmlCleanupParser();
    
    return outputString;
}

@end
